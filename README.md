# Software development course 2016

* [Course overview](https://gitlab.com/pccourse/software-development/blob/master/course_overview.md)
* [Part 1. Computers](https://gitlab.com/pccourse/software-development/tree/master/part_1)
* [Part 2. Language and memory parts](https://gitlab.com/pccourse/software-development/tree/master/part_2)
* [Part 3. Recursion, objects and references](https://gitlab.com/pccourse/software-development/tree/master/part_3)